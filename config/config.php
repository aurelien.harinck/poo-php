<?php
/**
 * Created by PhpStorm.
 * User: aurelienharinck
 * Date: 26/09/2018
 * Time: 17:26
 */

use Framework\Renderer\RendererInterface;
use Framework\Renderer\TwigRendererFactory;

return [
    'database.host' => 'localhost',
    'database.user' => 'root',
    'database.password' => '',
    'database.name' => 'blog',
    'views.path' => dirname(__DIR__) . '/views',
    'twig.extensions' => [
        \DI\get(\Framework\Router\RouterTwigExtension::class)
    ],
    \Framework\Router\Route::class => \DI\object(),
    RendererInterface::class => \DI\factory(TwigRendererFactory::class)
];
