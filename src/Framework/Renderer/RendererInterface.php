<?php
/**
 * Created by PhpStorm.
 * User: aurelienharinck
 * Date: 26/09/2018
 * Time: 15:26
 */

namespace Framework\Renderer;

interface RendererInterface
{
    /**
     * @param string $namespace
     * @param null|string $path
     */
    public function addPath(string $namespace, ?string $path = null): void;

    /**
     * @param string $view
     * @param array $params
     * @return string
     */
    public function render(string $view, array $params = []): string;

    /**
     * Permet d'ajouter des params globals à toutes les vues
     * @param string $key
     * @param mixed $value
     */
    public function addGlobal(string $key, $value): void;
}
