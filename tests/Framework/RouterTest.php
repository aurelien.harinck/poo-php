<?php

namespace Tests\Framework;

use Framework\Router;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase {

    /**
     * @var Router
     */
    private $router;

    public function setUp() {
        $this->router = new Router();
    }

    public function getMethod() {
        $request = new ServerRequest('GET', '/blog');
        $this->router->get('/blog', function() {return 'hello';}, 'blog');
        $route = $this->router->match($request);

        $this->assertEquals('blog', $route->getName());
        $this->assertEquals('blog', call_user_func($route->getCallback(), [$request]));
    }

    public function getMethodIfURLDoestNotExist() {
        $request = new ServerRequest('GET', '/blog');
        $this->router->get('/blogaze', function() {return 'hello';}, 'blog');
        $route = $this->router->match($request);

        $this->assertEquals(null, $route->getName());
    }

    public function testGetMethodWithParameters() {
        $request = new ServerRequest('GET', '/blog/mon-slug-8');
        $this->router->get('/blog', function() {return 'azazaz';}, 'posts');
        $this->router->get('/blog/{slug:[a-z0-9\-]+}-{id:\d+}', function() {return 'hello';}, 'post.show');
        $route = $this->router->match($request);

        $this->assertEquals('post.show', $route->getName());
        $this->assertEquals('hello', call_user_func($route->getCallback(), [$request]));
        $this->assertEquals(['slug' => 'mon-slug', 'id' => '8'], $route->getParams());

        // test invalide URL
        $route = $this->router->match(new ServerRequest('GET', '/blog/mon-slug_8'));
        $this->assertEquals(null, $route);
    }

    public function testGenerateUri() {

        $this->router->get('/blog', function() {return 'azazaz';}, 'posts');
        $this->router->get('/blog/{slug:[a-z0-9\-]+}-{id:\d+}', function() {return 'hello';}, 'post.show');

        $uri = $this->router->generateUri('post.show', ['slug' => 'mon-article', 'id' => 18]);

        $this->assertEquals('/blog/mon-article-18', $uri);
    }
}