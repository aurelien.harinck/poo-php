<?php
/**
 * Created by PhpStorm.
 * User: aurelienharinck
 * Date: 22/09/2018
 * Time: 15:50
 */

namespace Tests\Framework\Modules;

class ErroredModule {
    public function __construct(\Framework\Router $router)
    {
        $router->get('/demo', function() {
            return new \stdClass();
        }, 'demo');
    }
}