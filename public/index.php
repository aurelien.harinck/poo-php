<?php
/**
 * Created by PhpStorm.
 * User: aurelienharinck
 * Date: 21/09/2018
 * Time: 16:51
 */

require dirname(__DIR__) . '/vendor/autoload.php';

$modules = [
    \App\Blog\BlogModule::class
];

$builder = new \DI\ContainerBuilder();
$builder->addDefinitions(dirname(__DIR__) . DIRECTORY_SEPARATOR . "config/config.php");

foreach ($modules as $module) {
    if ($module::DEFINITIONS) {
        $builder->addDefinitions($module::DEFINITIONS);
    }
}

$builder->addDefinitions(dirname(__DIR__) . DIRECTORY_SEPARATOR . "config.php");

$container = $builder->build();

$renderer = $container->get(\Framework\Renderer\RendererInterface::class);

$app = new \Framework\App($container, $modules);

if (php_sapi_name() !== 'cli') {
    $response = $app->run(\GuzzleHttp\Psr7\ServerRequest::fromGlobals());
    \Http\Response\send($response);
}
